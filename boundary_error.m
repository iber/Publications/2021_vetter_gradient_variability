warning('off', 'MATLAB:table:ModifiedAndSavedVargradients')

gradients = {'pSmad', 'GBSGFP'};
gradients2 = {'pSmad', 'GBS_GFP'};
genes = {'dorsal Nkx6.1', 'ventral Pax3'};
genes2 = {'Nkx61', 'Pax3'};

diam = 4.9; % cell diameter in µm

t = (5:10:65)'; % time in hours
SS = t/2; % somite stage
L = 88.1*exp(t/41.6); % neural tube length in µm

tp = 1:3; % timepoints to plot

max_scaling_error = 50/(2*sqrt(3)); % artificial error in µm at the ventral end from domain scaling in Zagorski et al., 2017

% sample numbers for pSMAD & GBS-GFP
n = [24 30
     40 62
     46 69
     19 17
     55 42
     33 32
     19 21];

figure('Position', [0 0 800 1000])
gc = 'rg'; % gradient colors
bc = 'bk'; % boundary colors
m = '^s'; % markers

% positional error of domain boundaries as published in Zagorski et al., 2017
boundaries = readtable('domain_boundaries.csv');
for i = 1:numel(genes)
    dbe = boundaries.(['sd_' genes2{i} '_celldiam']);
    
    subplot(2, numel(genes), i)
    box on
    hold on
    title(genes{i})
    xlabel('Developmental time [SS]')
    ylabel('Positional error [cell diameter]')
    set(gca, 'FontSize', 18, 'LineWidth', 1)
    ylim([0 15])
    legend('Location', 'northwest')
    plot(SS(tp), dbe(tp), [bc(i) 'o-'], 'LineWidth', 2, 'MarkerFaceColor', bc(i), 'MarkerSize', 10, 'DisplayName', genes{i})
    
    subplot(2, numel(genes), numel(genes)+i)
    box on
    hold on
    title([genes{i} ', corrected'])
    xlabel('Developmental time [SS]')
    ylabel('Positional error [cell diameter]')
    set(gca, 'FontSize', 18, 'LineWidth', 1)
    ylim([0 15])
    legend('Location', 'northwest')
    plot(SS(tp), dbe(tp), [bc(i) 'o-'], 'LineWidth', 2, 'MarkerFaceColor', bc(i), 'MarkerSize', 10, 'DisplayName', genes{i})
end
        
% gradient positional error as published in Zagorski et al., 2017, interpolated at the boundary position
for j = 1:numel(gradients)
    err = readtable([gradients{j} '_error.csv']); % x=0 is the source, error is in cell diameters
    for i = 1:numel(genes)
        x = boundaries.(['x_rel_' genes2{i}]) .* L; % x=0 is the dorsal end in Zagorski et al., 2017
        scaling_error = max_scaling_error * x./L;
        
        if j == 2
            x = L - x; % invert DV axis for GBS-GFP, such that x=0 is the source
        end
        
        sigma_x_interpolated = NaN(size(t));
        for k = 1:numel(t)
            sigma_x_interpolated(k) = interp1(err.x_rel * L(k), err.(['sigma_x_' num2str(t(k)) 'h']), x(k));
        end
        sigma_x_corrected = max(0, sigma_x_interpolated - scaling_error / diam);
        
        subplot(2, numel(genes), i)
        plot(SS(tp), sigma_x_interpolated(tp), [gc(j) m(j) ':'], 'LineWidth', 2, 'MarkerFaceColor', gc(j), 'MarkerSize', 10, 'DisplayName', [gradients{j} ', Zagorski'])
        
        subplot(2, numel(genes), numel(genes)+i)
        plot(SS(tp), sigma_x_corrected(tp), [gc(j) m(j) ':'], 'LineWidth', 2, 'MarkerFaceColor', gc(j), 'MarkerSize', 10, 'DisplayName', [gradients{j} ', Zagorski'])
    end
end

drawnow

% true gradient positional error
nC = 10; % number of thresholds to sample
for j = 1:numel(gradients)
    sigma_x_interpolated = NaN(numel(t),numel(genes));
    sigma_x_corrected = NaN(numel(t),numel(genes));
    for k = 1:numel(t)
        max_C = NaN(n(k,j),1);
        for i = 1:n(k,j)
            % read in individual gradients (inquire the data from James Briscoe)
            G = readtable(['gradients/wt_' gradients{j} '_t' num2str(t(k)) 'h_id' num2str(i) '.dat']);
            max_C(i) = max(G.(gradients2{j}));
        end
        C_theta = logspace(-2, log10(mean(max_C)), nC);
        mu_x = NaN(nC,1);
        sigma_x = NaN(nC,1);
        for a = 1:nC
            x_theta = NaN(n(k,j),1);
            for i = 1:n(k,j)
                G = readtable(['gradients/wt_' gradients{j} '_t' num2str(t(k)) 'h_id' num2str(i) '.dat']);
                x = G.DV_microns;
                C = G.(gradients2{j});

                if j == 2
                    x = L(k) - x; % invert DV axis for GBS-GFP, such that x=0 is the source
                end
                
                % find the exact locations where the threshold is crossed
                idx = find(diff(sign(C - C_theta(a))) ~= 0)';
                x_zone = [];
                for b = idx
                    x_zone = [x_zone; interp1([C(b) C(b+1)], [x(b) x(b+1)], C_theta(a))];
                end
                if numel(x_zone) > 0
                    x_theta(i) = (min(x_zone) + max(x_zone)) / 2;
                    %x_theta(i) = mean(x_zone);
                    %x_theta(i) = median(x_zone);
                end
            end
            mu_x(a) = nanmean(x_theta);
            sigma_x(a) = nanstd(x_theta);
        end
        
        % interpolate the list of (mu_x,sigma_x) pairs to get the error at the boundary position
        [mu_x, idx] = sort(mu_x);
        sigma_x = sigma_x(idx);
        
        for i = 1:numel(genes)
            x = boundaries.(['x_rel_' genes2{i}]) .* L; % x=0 is the dorsal end in Zagorski et al., 2017
            scaling_error = max_scaling_error * x./L;
            
            if j == 2
                x = L - x; % invert DV axis for GBS-GFP, such that x=0 is the source
            end
            
            sigma_x_interpolated(k,i) = interp1(mu_x, sigma_x, x(k));
            sigma_x_corrected(k,i) = max(0, sigma_x_interpolated(k,i) - scaling_error(k));
        end
    end
    
    for i = 1:numel(genes)
        subplot(2, numel(genes), i)
        plot(SS(tp), sigma_x_interpolated(tp,i) / diam, [gc(j) m(j) '-'], 'LineWidth', 2, 'MarkerFaceColor', gc(j), 'MarkerSize', 10, 'DisplayName', [gradients{j} ', true'])
        
        subplot(2, numel(genes), numel(genes)+i)
        plot(SS(tp), sigma_x_corrected(tp,i) / diam, [gc(j) m(j) '-'], 'LineWidth', 2, 'MarkerFaceColor', gc(j), 'MarkerSize', 10, 'DisplayName', [gradients{j} ', true'])
    end
    
    drawnow
end
