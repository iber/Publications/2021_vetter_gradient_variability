gradients = {'GBSGFP', 'pSmad'};
colors = [0 1 0; 1 0 0];
diam = 4.9; % cell diameter in µm

figure('Position', [0 0 600 500])
box on
hold on
grid on
xlabel('True positional error [cell diameter]')
ylabel('Zagorski positional error [cell diameter]')
set(gca, 'FontSize', 18, 'LineWidth', 1, 'XScale', 'log', 'YScale', 'log')
xlim([1 30])
ylim([1 1e4])
plot([1 100], [1 100], 'k-', 'LineWidth', 2)

for j = 1:2
    param = readtable([gradients{j} '_parameters.csv']);

    for k = 7:-1:1
        t = param.time_h(k); % time in hours
        L = 88.1*exp(t/41.6); % neural tube length in µm

        % generate a synthetic set of exponential gradients with parameter statistics from Zagorski et al., Science 2017
        N = 10000; % number of gradients to generate
        pd = makedist('Normal', 'mu', param.lambda(k), 'sigma', param.lambda_sd(k));
        tpd = truncate(pd, 0, inf);
        lambda = random(tpd, N, 1);
        C0_sd_log = sqrt(log(1 + (param.C0_sd(k) / param.C0(k))^2));
        C0_mean_log = -C0_sd_log.^2 / 2;
        C0 = exp(normrnd(C0_mean_log, C0_sd_log, N, 1));

        % average the gradients
        x = linspace(0, L, 500)';
        C = @(i) C0(i) * exp(-x / lambda(i));
        mean_C = zeros(size(x));
        for i = 1:N
            mean_C = mean_C + C(i);
        end
        mean_C = mean_C / N;
        
        % lin-fit an exponential to mean(C)
        p = nlinfit(x, mean_C, @(p,x) p(1) * exp(-x/p(2)), [1, 20]);
        C0_fit = p(1);
        lambda_fit = p(2);
        
        % find the range of readout thresholds required to cover such that the entire DV axis is used
        C_min = param.C0(k) / 2;
        while mean(lambda .* log(C0 ./ C_min)) < L
            C_min = C_min / 2;
        end
        
        % direct calculation of the true error
        C_theta = logspace(log10(param.C0(j)), log10(C_min), 500)';
        mu_x = NaN(size(C_theta));
        sigma_x_true = NaN(size(C_theta));
        for i = 1:numel(C_theta)
            x_theta = lambda .* log(C0 ./ C_theta(i));
            mu_x(i) = mean(x_theta);
            sigma_x_true(i) = std(x_theta);
        end

        % vertical gradient variability
        sigma_C = NaN(size(mu_x));
        for i = 1:numel(mu_x)
            sigma_C(i) = std(C0 .* exp(-mu_x(i) ./ lambda));
        end

        % error propagation with lin-fitted parameters
        C_fit = C0_fit * exp(-mu_x / lambda_fit);
        sigma_x_Zagorski = lambda_fit ./ C_fit .* sigma_C;
        
        % restrict data to x_rel in [0,1]
        idx = find(mu_x < 0 | mu_x > L);
        sigma_x_true(idx) = [];
        sigma_x_Zagorski(idx) = [];

        % compare the positional errors
        plot(sigma_x_true / diam, sigma_x_Zagorski / diam, '-', 'Color', 1 - t/70 * (1 - colors(j,:)), 'LineWidth', 3)
    end
end
