# 2021_vetter_gradient_variability

R. Vetter & D. Iber, *Precision of morphogen gradients in neural tube development*
Nature Communications 13, 1145 (2022)
https://doi.org/10.1038/s41467-022-28834-3


## Contents

* positional_error_methods.m - MATLAB script that produces Fig. 1D
* positional_error_divergence.m - MATLAB script that produces Fig. 1E
* gradient_range.m - MATLAB script that produces Fig. 2A,B
* boundary_error.m - MATLAB script that produces Fig. 2C,D,E,H,I,J
* gradient_variability.m - MATLAB script for the simulation of morphogen gradient variability from molecular noise (Fig. 8)
* GBSGFP_error.csv - Table with positional errors [cell diameters] of GBS-GFP gradients, manually extracted from [1]
* pSmad_error.csv - Table with positional errors [cell diameters] of pSMAD gradients, manually extracted from [1]
* GBSGFP_parameters.csv - Table with statistical parameters of GBS-GFP gradients, manually extracted from [1]
* pSmad_parameters.csv - Table with statistical parameters of pSMAD gradients, manually extracted from [1]
* domain_boundaries.csv - Table with NKX6.1 and PAX3 domain boundary positions, manually extracted from [1]
* LICENSE - License file.

[1] Zagorski et al., Science 356, 1379-1383 (2017)


## Installation, System requirements

The MATLAB scripts were produced with version R2020a and require the Statistics and Machine Learning Toolbox (stats). No further installations are required, and no further system limitations apply. 


## Usage instructions

The MATLAB scripts can be executed out-of-the-box by just running them in MATLAB. 


## Input/Output

No manual specification of input or output is required, with the following exception:
gradient_range.m requires the raw gradient data from [1] as input, to be requested from Ani Kicheva or James Briscoe.
All MATLAB scripts produce as output the figures shown in the paper, as indicated in the list of contents above.


## Runtime

gradient_variability.m - The runtime to fully reproduce the morphogen gradient variability from molecular noise (Fig. 8) is a few days on a normal laptop computer. The runtime can be reduced proportionally by lowering the number of independent runs (nruns, line 12).
All other scripts run in a matter of seconds or less.


## License

All source code is released under the 3-Clause BSD license (see LICENSE for details).
