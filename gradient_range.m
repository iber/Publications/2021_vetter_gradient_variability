diam = 4.9; % cell diameter in µm

gradients = {'pSmad', 'GBSGFP'};
gradients2 = {'pSmad', 'GBS_GFP'};

t = 5:10:65; % time in hours

% sample numbers for pSMAD & GBS-GFP
n = [24 30
     40 62
     46 69
     19 17
     55 42
     33 32
     19 21];

warning('off', 'MATLAB:table:ModifiedAndSavedVargradients')
warning('off', 'MATLAB:Axes:NegativeDataInLogAxis')

close all
f1 = figure('Position', [0 0 2000 800]);
f2 = figure('Position', [0 0 500 500]);
for j = 1:2
    L = NaN(numel(t),1);
    x_limit = NaN(numel(t),1);
    x_limit_SE = NaN(numel(t),1);
    for k = 1:numel(t)
        allC = [];
        allx = [];
        for i = 1:n(k,j)
            % read in individual gradients (inquire the data from James Briscoe)
            G = readtable(['gradients/wt_' gradients{j} '_t' num2str(t(k)) 'h_id' num2str(i) '.dat']);
            x = G.DV_microns;
            x_rel = G.DV_rel;
            C = G.(gradients2{j});
            L(k) = x(end) / x_rel(end);
            
            if j == 2
                % invert DV axis for GBS-GFP such that x=0 is the source
                C = flipud(C);
            end
            
            allx = [allx; x];
            allC = [allC; C];
            
            figure(f1)
            subplot(2, numel(t), (j-1)*numel(t)+k)
            hold all
            box on
            grid on
            title([num2str(t(k)/2-2.5) '-' num2str(t(k)/2+2.5) ' somites'])
            xlabel('x [µm]')
            ylabel([gradients{j} ' [a.u.]'])
            plot(x, C)
            set(gca, 'FontSize', 18, 'LineWidth', 1, 'YScale', 'log')
        end
        
        % fit a two-piece linear function to log(C) with the kink position as free parameter
        idx = find(allC > 0);
        func = @(p,x) (x<p(1)).*(p(2)+p(3)*(x-p(1))) + (x>=p(1)).*(p(2)+p(4)*(x-p(1)));
        mdl = fitnlm(allx(idx), log(allC(idx)), func, [60 log(0.05) -1/20 0]);
        p = mdl.Coefficients.Estimate;
        x_limit(k) = p(1);
        x = [0 p(1) L(k)];
        plot(x, exp(func(p,x)), 'k-', 'LineWidth', 2)
        xlim([0 L(k)])
        text(p(1), exp(p(2)-0.5), [num2str(round(p(1))) ' µm'], 'FontSize', 18, 'HorizontalAlignment', 'center')
    end
    
    figure(f2)
    hold all
    box on
    xlabel('Developmental time [SS]')
    ylabel('DV axis [µm]')
    if j == 1
        % invert DV axis for pSMAD such that x=0 is the ventral end
        x_limit = L - x_limit;
    end
    c = 'rg';
    plot(t/2, x_limit, 'o-', 'LineWidth', 2, 'Color', c(j), 'MarkerFaceColor', c(j), 'MarkerSize', 10, 'DisplayName', [gradients{j} ' limit'])
    set(gca, 'FontSize', 18, 'LineWidth', 1)
    xlim([0 35])
    ylim([0 400])
end

plot(t/2, L, 'mo-', 'LineWidth', 2, 'MarkerFaceColor', 'm', 'MarkerSize', 10, 'DisplayName', 'dorsal NT end')

boundaries = readtable('domain_boundaries.csv');
plot(boundaries.time_h/2, (1-boundaries.x_rel_Pax3) .* L, 'ko-', 'LineWidth', 2, 'MarkerFaceColor', 'k', 'MarkerSize', 10, 'DisplayName', 'ventral PAX3')
plot(boundaries.time_h/2, (1-boundaries.x_rel_Nkx61) .* L, 'bo-', 'LineWidth', 2, 'MarkerFaceColor', 'b', 'MarkerSize', 10, 'DisplayName', 'dorsal NKX6.1')
legend('Location', 'northwest')