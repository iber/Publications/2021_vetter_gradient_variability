param = readtable('GBSGFP_parameters.csv');
err = readtable('GBSGFP_error.csv');

diam = 4.9; % cell diameter in µm

k = 4; % which timepoint to use (1-7)
t = param.time_h(k); % time in hours
L = 88.1*exp(t/41.6); % neural tube length in µm

% generate a synthetic set of exponential gradients with parameter statistics from Zagorski et al., Science 2017
N = 10000; % number of gradients to draw
pd = makedist('Normal', 'mu', param.lambda(k), 'sigma', param.lambda_sd(k));
tpd = truncate(pd, 0, inf);
lambda = random(tpd, N, 1);
C0_sd_log = sqrt(log(1 + (param.C0_sd(k) / param.C0(k))^2));
C0_mean_log = -C0_sd_log.^2 / 2;
C0 = exp(normrnd(C0_mean_log, C0_sd_log, N, 1));

% average the gradients
x_rel = (0:0.005:1)';
x = x_rel * L; % x=0 is the source (ventral end for GBS-GFP)
C = @(i) C0(i) * exp(-x / lambda(i));
mean_C = zeros(size(x));
for i = 1:N
    mean_C = mean_C + C(i);
end
mean_C = mean_C / N;

% lin-fit an exponential to mean(C)
p = nlinfit(x, mean_C, @(p,x) p(1)*exp(-x/p(2)), [1, 20]);
C0_fit = p(1);
lambda_fit = p(2);

% vertical gradient variability and the slope of the mean gradient
sigma_C = NaN(size(x));
slope = NaN(size(x));
for j = 1:length(x)
    Cj = C0 .* exp(-x(j) ./ lambda);
    sigma_C(j) = std(Cj);
    slope(j) = mean(-Cj ./ lambda);
end

% error propagation with mean slope
sigma_x_Gregor = sigma_C ./ abs(slope);

% error propagation with lin-fitted parameters
C_fit = C0_fit * exp(-x / lambda_fit);
sigma_x_Zagorski = lambda_fit ./ C_fit .* sigma_C;

% find the range of readout thresholds required to cover such that the entire DV axis is used
C_min = param.C0(k) / 2;
while mean(lambda .* log(C0 ./ C_min)) < L
    C_min = C_min / 2;
end

% direct calculation of the standard deviation
C_theta = logspace(log10(mean(C0)), log10(C_min), numel(x));
x_mean = NaN(size(x));
x_std = NaN(size(x));
for j = 1:length(C_theta)
    x_theta = lambda .* log(C0 ./ C_theta(j));
    x_mean(j) = mean(x_theta);
    x_std(j) = std(x_theta);
end
idx = find(x_mean >= 0 & x_mean <= L);
x_rel_true = x_mean(idx) / L;
sigma_x_true = x_std(idx);

% plot the data from Zagorski et al., Science 2017
figure('Position', [0 0 600 500])
plot(err.x_rel, err.(['sigma_x_' num2str(t) 'h']) * diam, 's:', 'Color', [0 0.6 0], 'MarkerFaceColor', [0 0.6 0], 'MarkerSize', 10, 'LineWidth', 1, 'DisplayName', 'Zagorski et al., Science 2017')
box on
hold on
xlabel('Relative DV position x/L')
ylabel('Positional error [µm]')
set(gca, 'FontSize', 18, 'LineWidth', 1)
ylim([0 150])
legend('Location', 'northwest')

% plot the different methods applied to the synthetic gradients
plot(x_rel_true, sigma_x_true, '-', 'Color', 'k', 'LineWidth', 2, 'DisplayName', 'True error')
plot(x_rel, sigma_x_Gregor, '--', 'Color', 'b', 'LineWidth', 2, 'DisplayName', 'Gregor method')
plot(x_rel, sigma_x_Zagorski, '-', 'Color', [0 0.6 0], 'LineWidth', 2, 'DisplayName', 'Zagorski method')
